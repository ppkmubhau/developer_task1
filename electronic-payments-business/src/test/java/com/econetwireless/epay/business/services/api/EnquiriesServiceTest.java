/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.econetwireless.epay.business.services.api;

import com.econetwireless.utils.messages.AirtimeBalanceResponse;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author PPKM
 */
public class EnquiriesServiceTest {
    
    public EnquiriesServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of enquire method, of class EnquiriesService.
     */
    @Test
    public void testEnquire() {
        System.out.println("enquire");
        String partnerCode = "";
        String msisdn = "";
        EnquiriesService instance = new EnquiriesServiceImpl();
        AirtimeBalanceResponse expResult = null;
        AirtimeBalanceResponse result = instance.enquire(partnerCode, msisdn);
        assertEquals(expResult, result);
    
        partnerCode = "445567";
        msisdn = "";
        result = instance.enquire(partnerCode, msisdn);
        assertEquals(expResult, result);
        
        partnerCode = "445567";
        msisdn = "2636882";
        result = instance.enquire(partnerCode, msisdn);
        assertEquals(expResult, result);
    }

    public class EnquiriesServiceImpl implements EnquiriesService {

        public AirtimeBalanceResponse enquire(String partnerCode, String msisdn) {
            return null;
        }
    }
    
}
