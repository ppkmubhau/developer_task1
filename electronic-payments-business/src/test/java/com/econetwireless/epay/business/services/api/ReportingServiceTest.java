/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.econetwireless.epay.business.services.api;

import com.econetwireless.epay.domain.SubscriberRequest;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author PPKM
 */
public class ReportingServiceTest {
    
    public ReportingServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of findSubscriberRequestsByPartnerCode method, of class ReportingService.
     */
    @Test
    public void testFindSubscriberRequestsByPartnerCode() {
        System.out.println("findSubscriberRequestsByPartnerCode");
        String partnerCode = "223345";
        ReportingService instance = new ReportingServiceImpl();
        List<SubscriberRequest> expResult = null;
        List<SubscriberRequest> result = instance.findSubscriberRequestsByPartnerCode(partnerCode);
        assertEquals(expResult, result);
        
    }

    public class ReportingServiceImpl implements ReportingService {

        public List<SubscriberRequest> findSubscriberRequestsByPartnerCode(String partnerCode) {
            return null;
        }
    }
    
}
