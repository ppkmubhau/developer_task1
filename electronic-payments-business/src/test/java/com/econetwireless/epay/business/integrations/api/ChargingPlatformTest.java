/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.econetwireless.epay.business.integrations.api;

import com.econetwireless.utils.pojo.INBalanceResponse;
import com.econetwireless.utils.pojo.INCreditRequest;
import com.econetwireless.utils.pojo.INCreditResponse;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author PPKM
 */
public class ChargingPlatformTest {
    
    public ChargingPlatformTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of enquireBalance method, of class ChargingPlatform.
     */
    @Test
    public void testEnquireBalance() {
        System.out.println("enquireBalance");
        String partnerCode = "";
        String msisdn = "";
        ChargingPlatform instance = new ChargingPlatformImpl();
        INBalanceResponse expResult = null;
        INBalanceResponse result = instance.enquireBalance(partnerCode, msisdn);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of creditSubscriberAccount method, of class ChargingPlatform.
     */
    @Test
    public void testCreditSubscriberAccount() {
        System.out.println("creditSubscriberAccount");
        INCreditRequest inCreditRequest = null;
        ChargingPlatform instance = new ChargingPlatformImpl();
        INCreditResponse expResult = null;
        INCreditResponse result = instance.creditSubscriberAccount(inCreditRequest);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    public class ChargingPlatformImpl implements ChargingPlatform {

        public INBalanceResponse enquireBalance(String partnerCode, String msisdn) {
            return null;
        }

        public INCreditResponse creditSubscriberAccount(INCreditRequest inCreditRequest) {
            return null;
        }
    }
    
}
